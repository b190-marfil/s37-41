const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseController.js");
const auth = require("../auth.js");


/*router.post("/",(req,res)=>{
	courseController.addCourse(req.body).then(resultFromController=> res.send(resultFromController));
});*/

// ACTIVITY S38


router.post("/",auth.verify,(req,res)=>{
	const userData = auth.decode(req.headers.authorization);
	// console.log(userData);
	// console.log(userData.isAdmin);
	courseController.addCourse(req.body,userData.isAdmin).then(resultFromController=>{res.send(resultFromController)});

});

router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController=>{res.send(resultFromController)});
});


router.get("/",(req,res)=>{
	courseController.getAllActiveCourses().then(resultFromController=>{res.send(resultFromController)});
});

router.get("/:courseId",(req,res) =>{
	courseController.getCourse(req.params).then(resultFromController=>{res.send(resultFromController)});
});

router.put("/:courseId",auth.verify,(req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=>{res.send(resultFromController)});
});

// ACTIVITY S40
router.put("/:courseId/a",auth.verify,(req,res)=>{
	courseController.archiveCourse(req.params).then(resultFromController=>{res.send(resultFromController)});
});

module.exports = router;
