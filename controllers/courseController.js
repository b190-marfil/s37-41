const Course = require("../models/Course.js");
const auth = require("../auth.js");


module.exports.addCourse = (reqBody,isAdmin)=>{
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});
	// console.log(isAdmin);
	if(isAdmin === false){
		return newCourse.delete().then(course =>{
			return false
		});
	}else{
		return newCourse.save().then(course => {
			return true;
		});	
	}
};

module.exports.getAllCourses = () =>{
	return Course.find({}).then(result=>{
		return result;
	})
};

module.exports.getAllActiveCourses = () =>{
	return Course.find({isActive: true}).then(result=>{
		return result;
	})
};

module.exports.getCourse = (reqParams) =>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result;
	})
};

module.exports.updateCourse = (reqParams,reqBody)=>{
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId,updateCourse).then((course,err)=>{
		if(err){
			return false;
		}else{
			return true;
		}
	})
};

// ACTIVITY S40
module.exports.archiveCourse = (reqParams)=>{
	let updateActiveField = {
		isActive: false
	};
	console.log(reqParams);
	console.log(reqParams.courseId);
	return Course.findByIdAndUpdate(reqParams.courseId,updateActiveField).then((course,err)=>{
		if(err){
			return false;
		}else{
			return true;
		}
	})
};